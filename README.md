## Task Description
Let's imagine that you should create a "Calculator" application. But now you should do nothing related to its logical part. You should create its design using different approaches that you can try in a real application.

## Complete the Task
Create an activity with 3 buttons to redirect to different implementations of a calculator. You can use activities or an activity with fragments.

Create the layouts of a very simple calculator using:

1. FrameLayout
2. LinearLayout
3. ConstraintLayout