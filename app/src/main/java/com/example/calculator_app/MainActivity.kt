package com.example.calculator_app

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.calculator_app.databinding.ActivityMainBinding
import com.example.calculator_app.fragment.MenuFragment

class MainActivity : AppCompatActivity(), Navigator {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportFragmentManager.beginTransaction().
                add(R.id.container, MenuFragment()).
                commit()

//        binding.toFrameLayoutBtn.setOnClickListener {
//            replaceFragment(FrameLayoutFragment())
//        }
//
//        binding.toConstraintLayoutBtn.setOnClickListener {
//            replaceFragment(ConstraintLayoutFragment())
//        }
//
//        binding.toLinearLayoutBtn.setOnClickListener {
//            replaceFragment(LinearLayoutFragment())
//        }
    }

    override fun replaceFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, fragment)
            .addToBackStack(null)
            .commit()
    }
}
interface Navigator{
    fun replaceFragment(fragment: Fragment)
}