package com.example.calculator_app.fragment

import android.content.Context
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.fragment.app.Fragment
import com.example.calculator_app.Navigator
import com.example.calculator_app.R


class MenuFragment: Fragment(R.layout.fragment_navigation) {
    private var navigator: Navigator? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        navigator = context as Navigator
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.findViewById<Button>(R.id.toFrameLayoutBtn).setOnClickListener {
            navigator?.replaceFragment(FrameLayoutFragment())
        }

        view.findViewById<Button>(R.id.toLinearLayoutBtn).setOnClickListener {
            navigator?.replaceFragment(LinearLayoutFragment())
        }

        view.findViewById<Button>(R.id.toConstraintLayoutBtn).setOnClickListener {
            navigator?.replaceFragment(ConstraintLayoutFragment())
        }
    }
}