package com.example.calculator_app.fragment

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageButton
import androidx.fragment.app.Fragment
import com.example.calculator_app.R
import com.example.calculator_app.databinding.FragmentLinearLayoutBinding

class LinearLayoutFragment: Fragment(R.layout.fragment_linear_layout) {
    private lateinit var binding: FragmentLinearLayoutBinding
    private var formula = ""
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.findViewById<TextView>(R.id.formula).text = formula

        view.findViewById<Button>(R.id.one_btn).setOnClickListener {
            formula = formula.plus("1")
            view.findViewById<TextView>(R.id.formula).text = formula
        }

        view.findViewById<Button>(R.id.two_btn).setOnClickListener {
            formula = formula.plus("2")

            view.findViewById<TextView>(R.id.formula).text = formula
        }
        view.findViewById<Button>(R.id.three_btn).setOnClickListener {
            formula = formula.plus("3")
            view.findViewById<TextView>(R.id.formula).text = formula
        }
        view.findViewById<Button>(R.id.four_btn).setOnClickListener {
            formula = formula.plus("4")
            view.findViewById<TextView>(R.id.formula).text = formula
        }
        view.findViewById<Button>(R.id.five_btn).setOnClickListener {
            formula = formula.plus("5")
            view.findViewById<TextView>(R.id.formula).text = formula
        }
        view.findViewById<Button>(R.id.six_btn).setOnClickListener {
            formula = formula.plus("6")
            view.findViewById<TextView>(R.id.formula).text = formula
        }
        view.findViewById<Button>(R.id.seven_btn).setOnClickListener {
            formula = formula.plus("7")
            view.findViewById<TextView>(R.id.formula).text = formula
        }
        view.findViewById<Button>(R.id.eight_btn).setOnClickListener {
            formula = formula.plus("8")
            view.findViewById<TextView>(R.id.formula).text = formula
        }
        view.findViewById<Button>(R.id.zero_btn).setOnClickListener {
            formula = formula.plus("0")
            view.findViewById<TextView>(R.id.formula).text = formula
        }
        view.findViewById<Button>(R.id.plus_btn).setOnClickListener {
            formula = formula.plus("+")
            view.findViewById<TextView>(R.id.formula).text = formula
        }
        view.findViewById<Button>(R.id.minus_btn).setOnClickListener {
            formula = formula.plus("-")
            view.findViewById<TextView>(R.id.formula).text = formula
        }
        view.findViewById<Button>(R.id.multiply_btn).setOnClickListener {
            formula = formula.plus("×")
            view.findViewById<TextView>(R.id.formula).text = formula
        }
        view.findViewById<Button>(R.id.delimiter_btn).setOnClickListener {
            formula = formula.plus("÷")
            view.findViewById<TextView>(R.id.formula).text = formula
        }
        view.findViewById<Button>(R.id.backspace_btn).setOnClickListener {
            formula = formula.dropLast(1)
            view.findViewById<TextView>(R.id.formula).text = formula
        }

        view.findViewById<Button>(R.id.remove_all_btn).setOnClickListener {
            formula = ""
            view.findViewById<TextView>(R.id.formula).text = formula
        }
        view.findViewById<Button>(R.id.equals_btn).setOnClickListener {
            view.findViewById<TextView>(R.id.result).text = calculateResult(formula).toString()
        }
    }


    fun calculateResult(expression: String): Double? {
        try {
            // Remove any whitespace from the expression
            val cleanedExpression = expression.replace("\\s".toRegex(), "")

            // Split the expression into tokens using regular expression
            val tokens = cleanedExpression.split("[-+÷×]".toRegex())

            // Validate the tokens and operator positions
            if (tokens.size < 2 || cleanedExpression.matches(".*[-+÷×]$".toRegex())) {
                println("Invalid expression format")
                return null
            }

            // Perform the calculation
            var result = tokens[0].toDouble()
            var index = 1
            while (index < tokens.size) {
                val operator = cleanedExpression[index]
                val operand = tokens[index].toDouble()
                result = when (operator) {
                    '+' -> result + operand
                    '-' -> result - operand
                    '×' -> result * operand
                    '÷' -> result / operand
                    else -> {
                        println("Invalid operator: $operator")
                        return null
                    }
                }
                index++
            }

            return result
        } catch (e: Exception) {
            println("Error occurred: ${e.message}")
            return null
        }
    }

}